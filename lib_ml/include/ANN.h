void init_rand();
double random_number(int allow_negatives);
double sigmoid(double x);
double sigmoid_derivative(double x);
void quicksort(int* arr, int pivot, int len);
double tanh_derivative(double x);

typedef struct
{
    double val;
    double activation_value;
    double* weights;//Weights Go Forward
    double delta;
    double bias;
    int length;
}FFNeuron;

typedef struct
{
    FFNeuron* neurons;
    int length;
}FFLayer;

typedef struct
{
    FFLayer* layers;
    int length;
}FFNetwork;

FFNetwork* FFCreateNetwork(uint32_t* shape, uint32_t length);
void FFFeedForward(FFNetwork* network, double* input, double(*activation)(double));
double FFBackprop(FFNetwork* network, double* desired, double(*activation_dirivative)(double), double learningRate);


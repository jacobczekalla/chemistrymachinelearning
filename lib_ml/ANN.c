#include <stdlib.h>
#include "include/ANN.h"

/**
 * @brief  creates a network struct for feed forward neural networks
 * @note   Be sure to free this memory eventually
 * @param  shape: A uint32_t array that describes the amount of layers and the node size. ex {2, 2, 2}.
 * @param  length: The length of the shape array
 * @retval Returns a pointer to network
 */
FFNetwork* FFCreateNetwork(uint32_t* shape, uint32_t length)
{
    FFNetwork* network = malloc(sizeof(FFNetwork));
    network->length = length;
    network->layers = malloc(sizeof (FFLayer) * network->length);

    for (uint32_t i = 0; i < network->length; i++) //For each layer
    {
        network->layers[i].length = shape[i];
        network->layers[i].neurons = malloc(sizeof(FFNeuron) * network->layers[i].length);
        for (uint32_t i1 = 0; i1 < network->layers[i].length; i1++) //For each neuron
        {
            network->layers[i].neurons[i1].bias = random_number(1);
            if(i != network->length-1)
                network->layers[i].neurons[i1].length = shape[i+1];
            else
                network->layers[i].neurons[i1].length = 0;
            network->layers[i].neurons[i1].weights = malloc(sizeof(double) * network->layers[i].neurons[i1].length);
            for (uint32_t i2 = 0; i2 < network->layers[i].neurons[i1].length; i2++)
                network->layers[i].neurons[i1].weights[i2] = random_number(1);
        }
    }
    return network;
}

/**
 * @brief  Runs the given network through a feed forward function. It processes the network
 * @note   This function is called before training or when an output is wanted
 * @param  network: The network to run. This network should be created through createdNetwork()
 * @param  input: The input to the neural network. Its length is assumed to be size of layer[0]
 * @retval None
 */
void FFFeedForward(FFNetwork* network, double* input, double(*activation)(double))
{
    for (uint32_t i = 0; i < network->length; i++)
    {
        //For each layer
        for (uint32_t i1 = 0; i1 < network->layers[i].length; i1++)
        {
            //For each neuron
            if(i == 0)
                network->layers[i].neurons[i1].val = input[i1];
            else
            {
                network->layers[i].neurons[i1].val = 0;
                network->layers[i].neurons[i1].activation_value = 0;
                //Calculate
                for (uint32_t i2 = 0; i2 < network->layers[i-1].length; i2++)
                {
                    network->layers[i].neurons[i1].val += network->layers[i-1].neurons[i2].val * network->layers[i-1].neurons[i2].weights[i1];
                }
                //Sigmoid
                network->layers[i].neurons[i1].activation_value = network->layers[i].neurons[i1].val + network->layers[i].neurons[i1].bias;
                network->layers[i].neurons[i1].val = activation(network->layers[i].neurons[i1].activation_value);
            }
        }
    }
}

//Returns old MSE
double FFBackprop(FFNetwork* network, double* desired, double(*activation_dirivative)(double), double learningRate)
{
    //uint32_t lastLayer = network->length-1;
    //uint32_t lastLayer_len = network->layers[lastLayer].length;
    double total_error = 0;

    for (int i = network->length-1; i >= 0; i--)//For each layer
    {
        for (int i1 = 0; i1 < network->layers[i].length; i1++)//For each neuron
        {
            if(i != network->length-1)
            {
                network->layers[i].neurons[i1].delta = 0;
                for (int i2 = 0; i2 < network->layers[i].neurons[i1].length; i2++)
                {
                    network->layers[i].neurons[i1].delta += network->layers[i+1].neurons[i2].delta * network->layers[i].neurons[i1].weights[i2];
                    network->layers[i].neurons[i1].weights[i2] -= learningRate * (network->layers[i+1].neurons[i2].delta * network->layers[i].neurons[i1].val);
                }
                network->layers[i].neurons[i1].delta *= activation_dirivative(network->layers[i].neurons[i1].val);
            }else
            {
                //Last layer
                network->layers[i].neurons[i1].delta = network->layers[i].neurons[i1].val - desired[i1];
                network->layers[i].neurons[i1].delta *= activation_dirivative(network->layers[i].neurons[i1].val);
            }
            network->layers[i].neurons[i1].bias -= learningRate * network->layers[i].neurons[i1].delta;
        }
    }
    return total_error;
}
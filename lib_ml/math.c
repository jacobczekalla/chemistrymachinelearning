#include <time.h>
#include <math.h>
#include <fcntl.h>
#include <stdlib.h>
#include "include/ANN.h"

void init_rand()
{
    srand(time(NULL));
}
double random_number(int negatives)
{
  double num;
  num = (double)rand() / (double)RAND_MAX;
  if (negatives)
  {
    num = (num * 2) - 1; // Transform range from [0,1] to [-1,1]
  }
  return num;
}

double sigmoid(double x)
{
    return (1/(1+exp(-x)));
}
double sigmoid_derivative(double x)
{
    return (x) * (1 - (x));
}

double tanh_derivative(double x)
{
    return 1 - pow(tanh(x), 2);
}

double meanSqusare(double* output, double* desired, uint32_t length)
{
    double error = 0;
    for (uint32_t i = 0; i < length; i++)
        error += pow((output[i] - desired[i]), 2);
    error = error / length;
    return error;
}

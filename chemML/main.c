#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ANN.h>
#include <math.h>
#include <grapher.h>

#define learningRate 1.7
#define trainingAmount 50000

#define activation sigmoid
#define activation_der sigmoid_derivative

//Training data set of 22
double training_vol[] = {10.0, 11.0, 11.6, 12.0, 12.5, 12.9, 13.1, 13.2, 13.3, 13.4, 13.48, 13.50, 13.60, 13.88, 13.98, 14.50, 15.10, 16.10, 17.10, 18.00, 21.00, 23.10 }; //mL
double training_ph[] =  {0.0472, 0.0495, 0.0510, 0.0527, 0.0554, 0.0583, 0.0628, 0.0725, 0.0852, 0.0929, 0.1041, 0.1099, 0.1130, 0.1168, 0.1179, 0.1206, 0.1226, 0.1243, 0.1255, 0.1264, 0.1281, 0.1288 }; //PH

void test(FFNetwork* network, int index)
{
    FFFeedForward(network, &(training_vol[index]), activation);
    printf("In: %lf | Out: %lf | Exp: %lf\n", training_vol[index], network->layers[network->length-1].neurons[0].val, training_ph[index]);
}
double run(FFNetwork* network, double input)
{
    FFFeedForward(network, &(input), activation);
    return network->layers[network->length-1].neurons[0].val;
}

int main()
{
    printf("Program started...\n");
    init_rand(); //Initialize random numbers
    uint32_t shape[] = {1, 1, 2, 3, 3, 1};
    printf("Creating Network...");
    FFNetwork* network = FFCreateNetwork(shape, 6);
    printf("Done.\n");
    printf("Testing network...\n");
    //Run for 10 times to verify...
    for (int i = 0; i < 5; i++)
        test(network, i);
    printf("Done.\n\n");

    printf("Training network...\n");
    for (int i1 = 0; i1 < trainingAmount; i1++)
    {
        for (int i = 0; i < 22; i++)
        {
            FFFeedForward(network, &(training_vol[i]), activation);
            FFBackprop(network, &(training_ph[i]), activation_der, learningRate);
        }
    }
    printf("Done.\n\n");
    for (int i = 0; i < 22; i++)
        test(network, i);

    double fake_x[400];
    double fake_y[400];
    for (int i = 0; i < 400; i++)
    {
        fake_x[i] = 0.1*(i)+10;
        fake_y[i] = 100*run(network, 0.1*i+10);
    }
    createGraphFromPoints(fake_x, fake_y, 400, "chart.html");
}